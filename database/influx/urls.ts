import {InfluxDBInjector} from "./injector";
import {IPoint, where} from "influx";
import {awaitExpression} from "babel-types";


export class Urls extends InfluxDBInjector {
    measurement = "url";

    constructor() {
        super();
    }

    public async insertNewUrl(inside) {
        const insert: IPoint = {
            fields: {
                shortCode: inside.shortCode,
                origin: inside.origin,
            },
            tags: {
                user_id: inside.user_id,
            }
        };
        return await this.db.writeMeasurement(this.measurement, [insert]);
    }

    public async getUrls(user_id) {
        return await this.db.query(`select * from url where \"user_id\"=\'${user_id}\'`)
    }

    public async getUrl(user_id, url_id) {
        return await this.db.query(`select * from url where \"user_id\"=\'${user_id}\' and \"shortCode\"=\'${url_id}\' limit 1`)
    }
}
import {InfluxDBInjector} from "./injector";
import {IPoint, IResults} from "influx";

export class Visits extends InfluxDBInjector{
    measurement = 'visit';
    constructor() {
        super();
    }

    public insertNewVisit(visit: UserVisit) {
        let insert: IPoint[] =  [{
            fields: {
                origin: visit.origin,
                url_id: visit.url_id,
                user_id: visit.user_id,
            },
            tags: {
                platform: visit.platform,
                os: visit.os,
                url_id: visit.url_id,
                referer: visit.referer
            }
        }];

        return this.db.writeMeasurement(this.measurement, insert)
    }

    public async getPieActivity(user_id: string, url_id: string, type: string): Promise<IResults<any>> {
        return await this.db.query(`select count(/url_id/) from visit where user_id =\'${user_id}\' and url_id=\'${url_id}'\  group by "${type}"`)
    }

    public async getTimeline(user_id: string, url_id: string, byTime) {
        return await this.db.query(`select count(/url_id/) from visit where user_id =\'${user_id}\' and url_id=\'${url_id}'\ group by time(${byTime})`)
    }

}


export interface UserVisit {
    platform: string;
    os: string;
    user_id: string;
    url_id: string;
    origin: string;
    referer: string;
}
import {FieldType, IClusterConfig, InfluxDB, IPoint} from "influx";
import {ISchemaOptions} from "influx/lib/src/schema";
import {ObjectID} from "bson";

const Influx = require('influx');
const config = {
    influxDB: {
        hosts: [{
            host: 'influx'
        }],
        database: 'short-url'
    }
};

export class InfluxDBInjector {

    protected db: InfluxDB;

    constructor() {
        const influx = new InfluxDB(config.influxDB);
        influx.getDatabaseNames()
            .then(names => {
                if (!names.includes(config.influxDB.database)) {
                    return influx.createDatabase(config.influxDB.database);
                } else return null;
            })
            .then(() => this.db = influx)
            .catch(err => console.error(err));
    }
}

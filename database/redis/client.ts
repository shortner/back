import {createClient, RedisClient} from "redis";
import * as redis from 'redis'
const {promisify} = require('util');


export class Redis {
    client: RedisClient;
    getAsync;
    constructor() {
        this.client = createClient(6379, "redis");
        this.client.on('error', (err) => {
            console.error(err)
        });
        this.getAsync = promisify(this.client.get).bind(this.client);
    }
    async set(hash: string, url: string) {
        const asyncSet = promisify(this.client.set).bind(this.client);
        return await asyncSet(hash, url)
    }

    async get(hash) {
        return await this.getAsync(hash)
    }
}
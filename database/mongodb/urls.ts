import {MongoDataBaseInjector} from "./injector";
import {InsertOneWriteOpResult, ObjectID} from "mongodb";
import {compareSync, hashSync} from "bcrypt";
import {sign, verify, decode} from "jsonwebtoken";

export class Users extends MongoDataBaseInjector{
    constructor() {
        super("urls");
    }
    public async newUser(email: string, password: string) {
        let user = await this.collection.findOne({email: email});
        console.log(email)
        if (user) {
            return {status: false, message: 'user is exist'}
        }
        user = await this.collection.insertOne({
            email: email,
            password: hashSync(password, 11),
            created_at: new Date()
        });
        console.log(user);
        const token = sign({user_id: user.insertedId}, 'secret');
        return {status: true, message: 'user is insert', token: token, username: email }
    }

    public async login(email: string, password: string) {
        let user = await this.collection.findOne({email: email});
        if (user && compareSync(password, user.password)) {
            const token = sign({user_id: user._id}, 'secret');
            return {status: true, message: 'good', username: user.email, token: token}
        }
        return {status: false, message: 'ne tuda!'}
    }

    public async findByID(id) {
        id = verify(id, 'secret');
        console.log(id)
        if (typeof id.user_id == 'string') { id = new ObjectID(id.user_id)}
        const user = await this.collection.findOne({_id: id});

        if (user) {
            return { status: true, message: 'good', username: user.email}
        } else {
            return { status: true, message: 'bad' }
        }

    }
}


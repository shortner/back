import {MongoClient, Collection} from 'mongodb'
const EventEmitter = require('events');

const config = {
    mongoDB: {
        url: "mongodb://mongo:27017",
        dbName: "url"
    }
};

export class MongoDataBaseInjector {

    private url = config.mongoDB.url;

    private dbName = config.mongoDB.dbName;

    protected collection: Collection;

    constructor(collectionName: string){
        this.getCollection(collectionName)
            .then(collection => this.collection = collection);
    }

    private async getCollection(collectionName: string): Promise<Collection> {
        try {
            let dbClient = await MongoClient.connect(this.url, {useNewUrlParser: true});
            return dbClient.db(this.dbName).collection(collectionName);
        } catch (e) {
            console.error(e);
        }
    }
}

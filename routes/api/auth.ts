import { Router, Request, Response } from 'express';
import {login, newUser, status } from '../../controllers/auth';

const router: Router = Router();

router.post('/sign-up', newUser);
router.post('/sign-in', login);
router.post('/status', status);

export const auth: Router = router;
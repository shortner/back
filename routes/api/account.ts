import { Router, Request, Response } from 'express';
import {createNewUrl, getUrls, getPageAnalytic, getUrlInfo } from "../../controllers/account";

const router: Router = Router();

router.get("/", getUrls);
router.post("/", createNewUrl);

//analytic
router.get('/analytic-page/:id', getUrlInfo);
router.get('/analytic/:id', getPageAnalytic);

export const account = router;

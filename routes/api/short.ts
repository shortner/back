import {Router} from "express";
import { redirectMiddleware } from "../../middleware/redirecter";
import {redirect} from "../../controllers/redirect";

const router: Router = Router();

router.get('/:code', redirectMiddleware, redirect);


export const short = router;
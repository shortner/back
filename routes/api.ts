import {Router} from "express";
import {shortApi, accountApi, authApi} from "./index";
import {JWT} from "../middleware/token";
import {redirectMiddleware} from "../middleware/redirecter";
import {angular} from "../controllers/redirect";

const router: Router = Router();

router.use('/api/account', JWT, accountApi);
router.use('/api/auth', authApi);
router.use('/q', redirectMiddleware, shortApi);
router.get('*', angular)

export const api = router;
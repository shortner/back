import {account} from "./api/account";
import {auth} from "./api/auth";
import {short} from "./api/short";


export const shortApi = short;
export const authApi = auth;
export const accountApi = account;
FROM node:carbon

ADD package.json .

RUN npm i
EXPOSE 3000

ADD . .

#RUN tsc


CMD ["npm", "start"]
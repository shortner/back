import {Redis} from "../database/redis/client";



export class Redirecter {
    client: Redis;

    constructor() {
        this.client = new Redis();
    }
    async read(req, res, next) {
        // if (!req.params.code) { return res.sendStatus(401); }
        const id = req.url.split("/")[1];
        const visitObject = await this.client.get(id);
        if (visitObject) {
            const payload = JSON.parse(visitObject);
            req.payload = {
                url_id: payload.url_id,
                user_id: payload.user_id,
                referer: this.parseReferer(req.headers["referer"])
            };
            next();
        } else {
            return res.sendStatus(404);
        }
    }

    parseReferer(referer) {
        const ref = referer ? referer: "unknown";
        const match = ref.match(/(https|http):\/\/[a-zA-Z-]+.[a-z]+/);
        if (referer && match) {
            return match[0]
        }
        return ref
    }
}

export async function redirectMiddleware(req, res, next) {
    const redirect = new Redirecter();
    await redirect.read(req, res, next)
}
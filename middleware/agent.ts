export class Agent {

    source;

    constructor() {
    }

    public parse(req, resp, next) {
        this.source = req.headers["user-agent"];
        if (this.source) {
            let browser = this.parseBrowser();
            let os = this.parseOS();
            req.device = {
                browser: browser,
                os: os
            };

            return next();
        } else {
            req.device = {
                browser: "unknown",
                os: "unknown"
            };
            next();
        }
    }

    private parseBrowser() {
        let data = this.source.match(/[a-zA-Z]+\/[0-9(|.)]+/gm);
        data = data.map((value) => {
            return value.split("/")[0]
        });
        const allowBrowsers = ["Chrome", "Opera", "Firefox", "Safari"];
        for (let browser of allowBrowsers) {
            if (data.includes(browser)) {
                return browser
            }
        }
        return "unknown"
    }

    private parseOS() {
        const data = this.source.match(/\(.*?\)/gmis)[0];
        const allowOS = ["Windows", "Mac", "Android", "Linux"];
        for (let os of allowOS) {
            if (data.includes(os)) {
                return os
            }
        }
        return "unknown"
    }
}

export function useragent() {

    return function (req, res, next) {
        const agent = new Agent();
        agent.parse(req, res, next);
    }
}
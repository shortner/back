import {sign, verify} from 'jsonwebtoken'

export  function  JWT(req, res, next) {
    try {
        let token = req.headers.authorization.split(" ")[1];
        let answer = verify(token, 'secret');

        req.user = answer['user_id'];
        next()
    } catch (e) {
        res.sendStatus(401)
    }
}
import {Visits} from "../database/influx/visits";
import * as path from 'path'
const visits = new Visits();

export async function redirect(req, res) {
    await visits.insertNewVisit({
        origin: req.payload.url_id,
        os: req.device.os,
        platform: req.device.browser,
        user_id: req.payload.user_id,
        url_id: req.params.code,
        referer: req.payload.referer,
    });
    res.redirect(req.payload.url_id);
}

export async function angular(req, res) {
    res.header('Cache-Control', "max-age=60, must-revalidate, private");
    res.sendFile( path.join(__dirname, 'index.html') );
}
import { Users } from "../database/mongodb/urls";

const urls = new Users();

export async function login(req, res) {
    try {
        const result = await urls.login(req.body.email, req.body.password);
        res.json(result)
    } catch (e) {
        console.error(e);
        // res.sendStatus(404)
    }
}

export async function newUser(req, res) {
    try {
        const result = await urls.newUser(req.body.email, req.body.password);
        res.json(result)
    } catch (e) {
        console.error(e);
        res.sendStatus(404)

    }
}

export async function status(req, res) {
    try {
        if (!req.body.token) { req.sendStatus(401)}
        const result = await urls.findByID(req.body.token);
        res.json(result)
    } catch (e) {
        console.error(e);
        res.sendStatus(401)
    }
}

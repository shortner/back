import { Request, Response} from "express";
import { Redis } from "../database/redis/client";
import { Visits } from "../database/influx/visits";
import {Urls} from "../database/influx/urls";

const redis = new Redis();
const urls = new Urls();
const visits = new Visits();

export async function createNewUrl(req, res: Response) {
    if (!req.body.origin) {res.sendStatus(400)}
    let url = req.user + "-" + req.body.origin;
    let check = checkSums(url);
    const urlRecord = await redis.get(check);

    if (urlRecord) {
        res.json({
            status: false,
            shortCode: null,
            message: 'url is exist'
        });
    } else {
        await redis.set(check, JSON.stringify({
            url_id: req.body.origin,
            user_id: req.user
        }));
        await urls.insertNewUrl({
            shortCode: check,
            origin: req.body.origin,
            user_id: req.user,
        });
        res.json({
            status: true,
            shortCode: check,
            message: "",
        });
    }
}


export async function getUrls(req, res) {
    try {
        res.json({
            result: await urls.getUrls(req.user)
        })
    } catch (e) {
        console.error(e);
        res.sendStatus(400);
    }
}


export async function getPageAnalytic(req, res) {
    const os = await visits.getPieActivity(req.user, req.params.id, "os");
    const platform = await visits.getPieActivity(req.user, req.params.id, "platform");
    const referer = await visits.getPieActivity(req.user, req.params.id, "referer");
    const timeline = await visits.getTimeline(req.user, req.params.id, '1m');
    res.json({os: os, platform: platform , timeline: timeline, referer: referer})
}

export async function getUrlInfo(req, res) {
    const url = await urls.getUrl(req.user, req.params.id);
    return res.json({result: url})
}


function checkSums(s) {
  let chk = 0x12345678;
  const len = s.length;
  for (let i = 0; i < len; i++) {
      chk += (s.charCodeAt(i) * (i + 1));
  }
  return (chk & 0xffffffff).toString(16);
}
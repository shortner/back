(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./src/$$_lazy_route_resource lazy recursive":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error('Cannot find module "' + req + '".');
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/about/about.component.html":
/*!********************************************!*\
  !*** ./src/app/about/about.component.html ***!
  \********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"flex-box\">\n\n  <div class=\"warning\">\n    <mat-icon>warning</mat-icon>\n    Starting March 30, 2018, we will be turning down support for yand.ex URL shortener. From\n    only existing users will be able to create short links on the yand.ex console. You will be able to view your analytics data and download your short link information in csv format for up to one year, until\n    , when we will discontinue yand.ex. Previously created links will continue to redirect  to their intended destination. Please see this\n    <a href=\"//developers.googleblog.com/2018/03/transitioning-google-url-shortener.html\">\n      blog post\n      for more details.\n    </a>\n  </div>\n  <div class=\"content\">\n    <div class=\"content-item\">\n      <div  class=\"item\">\n        <div class=\"icon\">\n          <mat-icon class=\"icon-link link-color\">link</mat-icon>\n        </div>\n        <div class=\"header\">\n          Shorten\n        </div>\n      </div>\n      Analytics help you know where your clicks are coming from\n    </div>\n    <div class=\"content-item\">\n      <div  class=\"item\">\n        <div class=\"icon\">\n          <mat-icon class=\"icon-link track-color\">trending_up</mat-icon>\n        </div>\n        <div class=\"header\">\n          Track\n        </div>\n      </div>\n      Analytics help you know where your clicks are coming from\n    </div>\n    <div class=\"content-item\">\n      <div  class=\"item\">\n        <div class=\"icon\">\n          <mat-icon class=\"icon-link people-color\">people</mat-icon>\n        </div>\n        <div class=\"header\">\n          Learn\n        </div>\n      </div>\n      Understand and visualize your audience\n    </div>\n  </div>\n  <div class=\"developer\">\n    <img src=\"https://goo.gl/static/Firebase.png\" class=\"firebase\">\n    <div class=\"about-firebase\">\n      <h2>Are you an app developer?</h2>\n      <span>Use Firebase Dynamic Links to deep link users directly into your app.<a href=\"//firebase.google.com/docs/dynamic-links/\" >Learn More <mat-icon class=\"icon-link-firebase\">open_in_new</mat-icon></a></span>\n    </div>\n  </div>\n\n</div>\n"

/***/ }),

/***/ "./src/app/about/about.component.scss":
/*!********************************************!*\
  !*** ./src/app/about/about.component.scss ***!
  \********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".flex-box {\n  display: flex;\n  flex-wrap: wrap;\n  font-family: Roboto, \"Helvetica Neue\", sans-serif;\n  font-weight: 100;\n  flex-direction: column;\n  padding: 2em 210px;\n  color: #757575; }\n\n.warning {\n  margin: 20px 0;\n  background-color: #f9eebc;\n  border-radius: 25px;\n  padding: 20px;\n  font-style: italic; }\n\n.icon-link {\n  width: 2em;\n  height: 2em;\n  line-height: 70px;\n  border-radius: 50%;\n  /* the magic */\n  -moz-border-radius: 50%;\n  -webkit-border-radius: 50%;\n  text-align: center;\n  font-size: 35px;\n  font-weight: 700;\n  margin: 0 auto 40px; }\n\n.link-color {\n  color: #29b6f6;\n  background-color: #e1f5fe; }\n\n.content {\n  display: flex;\n  flex-direction: row;\n  flex-flow: wrap; }\n\n.content > .content-item {\n  width: 29%;\n  padding-right: 2%; }\n\n.header {\n  padding-top: 25px;\n  margin-left: 20px;\n  font-size: 1.5em;\n  font-weight: bold; }\n\n.item {\n  display: flex;\n  flex-direction: row; }\n\n.track-color {\n  color: #ba68c8;\n  background-color: #ede7f6; }\n\n.people-color {\n  color: #9ccc65;\n  background-color: #f1f8e9; }\n\n.developer {\n  margin-top: 20px;\n  background-color: #f0f3f4;\n  padding: 20px 20px;\n  display: flex;\n  flex-direction: row;\n  color: #757575; }\n\nimg {\n  width: 64px;\n  height: 64px;\n  padding: 10px 2px; }\n\n.icon-link-firebase {\n  font-size: 15px;\n  padding-top: 2px; }\n\na {\n  color: #366ed1; }\n\nh2 {\n  line-height: 10px; }\n\n@media only screen and (max-width: 989px) {\n  .flex-box {\n    padding: 40px; } }\n\n@media only screen and (max-width: 673px) {\n  .content > .content-item {\n    width: 100%;\n    margin-top: 10px; }\n  .developer {\n    flex-direction: column; }\n  img {\n    text-align: center;\n    margin: auto; } }\n"

/***/ }),

/***/ "./src/app/about/about.component.ts":
/*!******************************************!*\
  !*** ./src/app/about/about.component.ts ***!
  \******************************************/
/*! exports provided: AboutComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AboutComponent", function() { return AboutComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var AboutComponent = /** @class */ (function () {
    function AboutComponent() {
    }
    AboutComponent.prototype.ngOnInit = function () {
    };
    AboutComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-about',
            template: __webpack_require__(/*! ./about.component.html */ "./src/app/about/about.component.html"),
            styles: [__webpack_require__(/*! ./about.component.scss */ "./src/app/about/about.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], AboutComponent);
    return AboutComponent;
}());



/***/ }),

/***/ "./src/app/analytic/analytic.component.html":
/*!**************************************************!*\
  !*** ./src/app/analytic/analytic.component.html ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-header></app-header>\n<div class=\"content-padding\">\n  <div class=\"about-link\" *ngIf=\"about\">\n    <span class=\"header\">Analytics data for  <a href=\"https://localhost:2500/q/{{about.shorCode}}\">yand.ex/q/{{about.shortCode}}</a></span>\n    <span class=\"created\">Created at {{about.time}}</span>\n    <span class=\"original\">Original URL <a href=\"{{about.origin}}\">{{about.origin}}</a></span>\n  </div>\n\n\n</div>\n<app-chart [data]=\"data\"></app-chart>\n\n"

/***/ }),

/***/ "./src/app/analytic/analytic.component.scss":
/*!**************************************************!*\
  !*** ./src/app/analytic/analytic.component.scss ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".content-padding {\n  padding: 20px 210px;\n  background-color: #efeef3; }\n\n.about-link {\n  display: flex;\n  flex-direction: column; }\n\nspan {\n  font-weight: 300; }\n\nspan.header {\n  font-size: 1.2em;\n  margin: 5px 0; }\n\n.charts {\n  padding: 20px 210px;\n  display: flex;\n  flex-direction: row;\n  flex-flow: wrap; }\n\n.about-clicks {\n  display: flex;\n  flex-direction: column;\n  justify-content: center;\n  border: 0.5px black solid;\n  padding: 5px; }\n\n.timeframe {\n  display: flex;\n  flex-direction: column-reverse; }\n\n.timeframe-row {\n  display: flex;\n  flex-direction: row;\n  margin-left: 5px; }\n\n.total {\n  padding: 5px;\n  font-size: 0.8em;\n  margin: auto; }\n\nspan.clicks {\n  font-size: 1em;\n  font-weight: bold;\n  padding: 10px;\n  margin: auto; }\n\n.charts > .half-chart {\n  width: 50%;\n  min-height: 400px; }\n\n.charts > .full-chart {\n  width: 100%;\n  min-height: 150px; }\n\n.full-chart-about {\n  display: flex;\n  flex-direction: row;\n  flex-flow: wrap;\n  justify-content: space-between; }\n\n@media only screen and (max-width: 1000px) {\n  .content-padding {\n    padding: 40px 40px;\n    display: flex;\n    flex-direction: row;\n    flex-flow: wrap; } }\n"

/***/ }),

/***/ "./src/app/analytic/analytic.component.ts":
/*!************************************************!*\
  !*** ./src/app/analytic/analytic.component.ts ***!
  \************************************************/
/*! exports provided: AnalyticComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AnalyticComponent", function() { return AnalyticComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _service_charts_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../service/charts.service */ "./src/app/service/charts.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};



var AnalyticComponent = /** @class */ (function () {
    function AnalyticComponent(chartService, router) {
        var _this = this;
        this.chartService = chartService;
        this.router = router;
        this.data = {};
        this.router.params.subscribe(function (url) {
            _this.id = url.id;
        });
        this.chartService.getPage(this.id)
            .subscribe(function (res) {
            _this.about = res['result'][0];
        });
    }
    AnalyticComponent.prototype.ngOnInit = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.getAnalytic()];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    AnalyticComponent.prototype.getAnalytic = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            var result;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.chartService.getData(this.id)];
                    case 1:
                        result = _a.sent();
                        result.subscribe(function (data) {
                            _this.data = {
                                os: _this.normalize(data['os'], false, 'os'),
                                platform: _this.normalize(data['platform'], false, 'platform'),
                                timeline: _this.normalize(data['timeline'], true, 'timeline'),
                                referer: _this.normalize(data['referer'], false, 'referer'),
                                clicks: _this.getCount(data['platform'])
                            };
                        });
                        return [2 /*return*/];
                }
            });
        });
    };
    AnalyticComponent.prototype.normalize = function (data, series, name) {
        if (series) {
            var result = {
                name: this.id,
                series: []
            };
            result.series = data.map((function (value) {
                return { name: new Date(value.time), value: value.count_url_id };
            }));
            return result;
        }
        else {
            var result_ = [];
            result_ = data.map(function (value) {
                return { name: value[name], value: value.count_url_id };
            });
            return result_;
        }
    };
    AnalyticComponent.prototype.getCount = function (data) {
        if (data.length > 1) {
            return data.reduce(function (prev, cur) {
                if (typeof prev === 'object') {
                    return prev.count_url_id + cur.count_url_id;
                }
                return prev + cur.count_url_id;
            });
        }
        else {
            return data[0].count_url_id;
        }
    };
    AnalyticComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-analytic',
            template: __webpack_require__(/*! ./analytic.component.html */ "./src/app/analytic/analytic.component.html"),
            styles: [__webpack_require__(/*! ./analytic.component.scss */ "./src/app/analytic/analytic.component.scss")]
        }),
        __metadata("design:paramtypes", [_service_charts_service__WEBPACK_IMPORTED_MODULE_1__["ChartsService"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"]])
    ], AnalyticComponent);
    return AnalyticComponent;
}());

// function formateSeries(data) {
//   data = data.map((value) => {
//     return {
//       name: value[]
//     }
//   })
// }


/***/ }),

/***/ "./src/app/analytics.guard.ts":
/*!************************************!*\
  !*** ./src/app/analytics.guard.ts ***!
  \************************************/
/*! exports provided: AnalyticsGuard */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AnalyticsGuard", function() { return AnalyticsGuard; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _service_user_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./service/user.service */ "./src/app/service/user.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var AnalyticsGuard = /** @class */ (function () {
    function AnalyticsGuard(auth, router) {
        this.auth = auth;
        this.router = router;
    }
    AnalyticsGuard.prototype.canActivate = function (next, state) {
        var user = this.auth.getUser();
        if (user) {
            return true;
        }
        else {
            this.router.navigate(['/login'], {
                queryParams: {
                    return: state.url
                }
            });
            return false;
        }
    };
    AnalyticsGuard = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_service_user_service__WEBPACK_IMPORTED_MODULE_2__["UserService"], _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"]])
    ], AnalyticsGuard);
    return AnalyticsGuard;
}());



/***/ }),

/***/ "./src/app/app-routing.module.ts":
/*!***************************************!*\
  !*** ./src/app/app-routing.module.ts ***!
  \***************************************/
/*! exports provided: AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _analytic_analytic_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./analytic/analytic.component */ "./src/app/analytic/analytic.component.ts");
/* harmony import */ var _main_main_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./main/main.component */ "./src/app/main/main.component.ts");
/* harmony import */ var _login_login_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./login/login.component */ "./src/app/login/login.component.ts");
/* harmony import */ var _registration_registration_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./registration/registration.component */ "./src/app/registration/registration.component.ts");
/* harmony import */ var _analytics_guard__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./analytics.guard */ "./src/app/analytics.guard.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};







var routes = [
    { path: 'analytics/:id', canActivate: [_analytics_guard__WEBPACK_IMPORTED_MODULE_6__["AnalyticsGuard"]], component: _analytic_analytic_component__WEBPACK_IMPORTED_MODULE_2__["AnalyticComponent"] },
    { path: '', component: _main_main_component__WEBPACK_IMPORTED_MODULE_3__["MainComponent"] },
    { path: 'login', component: _login_login_component__WEBPACK_IMPORTED_MODULE_4__["LoginComponent"] },
    { path: 'registration', component: _registration_registration_component__WEBPACK_IMPORTED_MODULE_5__["RegistrationComponent"] }
];
var AppRoutingModule = /** @class */ (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forRoot(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], AppRoutingModule);
    return AppRoutingModule;
}());



/***/ }),

/***/ "./src/app/app.component.html":
/*!************************************!*\
  !*** ./src/app/app.component.html ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!--<app-about></app-about>-->\n<!--<app-main></app-main>-->\n<router-outlet></router-outlet>\n"

/***/ }),

/***/ "./src/app/app.component.scss":
/*!************************************!*\
  !*** ./src/app/app.component.scss ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var AppComponent = /** @class */ (function () {
    function AppComponent() {
        this.title = 'app';
    }
    AppComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-root',
            template: __webpack_require__(/*! ./app.component.html */ "./src/app/app.component.html"),
            styles: [__webpack_require__(/*! ./app.component.scss */ "./src/app/app.component.scss")]
        })
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _input_field_input_field_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./input-field/input-field.component */ "./src/app/input-field/input-field.component.ts");
/* harmony import */ var _header_header_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./header/header.component */ "./src/app/header/header.component.ts");
/* harmony import */ var _about_about_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./about/about.component */ "./src/app/about/about.component.ts");
/* harmony import */ var _link_table_link_table_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./link-table/link-table.component */ "./src/app/link-table/link-table.component.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/platform-browser/animations */ "./node_modules/@angular/platform-browser/fesm5/animations.js");
/* harmony import */ var _analytic_analytic_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./analytic/analytic.component */ "./src/app/analytic/analytic.component.ts");
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./app-routing.module */ "./src/app/app-routing.module.ts");
/* harmony import */ var _main_main_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./main/main.component */ "./src/app/main/main.component.ts");
/* harmony import */ var _swimlane_ngx_charts__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @swimlane/ngx-charts */ "./node_modules/@swimlane/ngx-charts/release/esm.js");
/* harmony import */ var _service_charts_service__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./service/charts.service */ "./src/app/service/charts.service.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _login_login_component__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./login/login.component */ "./src/app/login/login.component.ts");
/* harmony import */ var _signup_signup_component__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./signup/signup.component */ "./src/app/signup/signup.component.ts");
/* harmony import */ var _interceptor_auth_interceptor__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ./interceptor/auth.interceptor */ "./src/app/interceptor/auth.interceptor.ts");
/* harmony import */ var _service_user_service__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ./service/user.service */ "./src/app/service/user.service.ts");
/* harmony import */ var _modal_preview_preview_component__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! ./modal/preview/preview.component */ "./src/app/modal/preview/preview.component.ts");
/* harmony import */ var angularx_qrcode__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! angularx-qrcode */ "./node_modules/angularx-qrcode/dist/index.js");
/* harmony import */ var _chart_chart_component__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! ./chart/chart.component */ "./src/app/chart/chart.component.ts");
/* harmony import */ var _registration_registration_component__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! ./registration/registration.component */ "./src/app/registration/registration.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
























var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: [
                _app_component__WEBPACK_IMPORTED_MODULE_3__["AppComponent"],
                _input_field_input_field_component__WEBPACK_IMPORTED_MODULE_4__["InputFieldComponent"],
                _header_header_component__WEBPACK_IMPORTED_MODULE_5__["HeaderComponent"],
                _about_about_component__WEBPACK_IMPORTED_MODULE_6__["AboutComponent"],
                _link_table_link_table_component__WEBPACK_IMPORTED_MODULE_7__["LinkTableComponent"],
                _analytic_analytic_component__WEBPACK_IMPORTED_MODULE_10__["AnalyticComponent"],
                _main_main_component__WEBPACK_IMPORTED_MODULE_12__["MainComponent"],
                _login_login_component__WEBPACK_IMPORTED_MODULE_16__["LoginComponent"],
                _signup_signup_component__WEBPACK_IMPORTED_MODULE_17__["SignupComponent"],
                _modal_preview_preview_component__WEBPACK_IMPORTED_MODULE_20__["PreviewComponent"],
                _chart_chart_component__WEBPACK_IMPORTED_MODULE_22__["ChartComponent"],
                _registration_registration_component__WEBPACK_IMPORTED_MODULE_23__["RegistrationComponent"],
            ],
            imports: [
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__["BrowserModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatIconModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatButtonModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatTableModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatFormFieldModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_8__["FormsModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatInputModule"],
                _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_9__["NoopAnimationsModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatPaginatorModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatMenuModule"],
                _app_routing_module__WEBPACK_IMPORTED_MODULE_11__["AppRoutingModule"],
                _swimlane_ngx_charts__WEBPACK_IMPORTED_MODULE_13__["NgxChartsModule"],
                _angular_common_http__WEBPACK_IMPORTED_MODULE_15__["HttpClientModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatDividerModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatCardModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatInputModule"],
                _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_9__["BrowserAnimationsModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatProgressBarModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_8__["ReactiveFormsModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatDialogModule"],
                angularx_qrcode__WEBPACK_IMPORTED_MODULE_21__["QRCodeModule"]
            ],
            providers: [
                _service_charts_service__WEBPACK_IMPORTED_MODULE_14__["ChartsService"],
                { provide: _angular_common_http__WEBPACK_IMPORTED_MODULE_15__["HTTP_INTERCEPTORS"], useClass: _interceptor_auth_interceptor__WEBPACK_IMPORTED_MODULE_18__["AuthInterceptor"], multi: true },
                _service_user_service__WEBPACK_IMPORTED_MODULE_19__["UserService"]
            ],
            entryComponents: [_modal_preview_preview_component__WEBPACK_IMPORTED_MODULE_20__["PreviewComponent"]],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_3__["AppComponent"]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/chart/chart.component.html":
/*!********************************************!*\
  !*** ./src/app/chart/chart.component.html ***!
  \********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"charts\" *ngIf=\"data\">\n  <div class=\"full-chart\" *ngIf=\"data.timeline\">\n    <div class=\"full-chart-about\">\n      <div class=\"about-clicks\">\n        <span class=\"total\">Total clicks</span>\n        <span class=\"clicks\" (click)=\"getClicks()\">{{clicks | json}}</span>\n      </div>\n      <div class=\"timeframe\"><div class=\"timeframe-row\">group by  <span style=\"font-weight: bold\">1 min</span></div> </div>\n    </div>\n    <ngx-charts-line-chart\n      *ngIf=\"data.timeline\"\n      [results]=\"[data.timeline]\"\n      [scheme]=\"colors\"\n      timeline=\"true\"\n      xAxis=\"true\"\n      yAxis=\"true\"\n      yAxisLabel=\"timeline\"\n      showYAxisLabel=\"true\"\n    >\n    </ngx-charts-line-chart>\n  </div>\n  <div class=\"half-chart\">\n    <div>\n      <div>OS</div>\n      <mat-divider></mat-divider>\n    </div>\n    <ngx-charts-pie-chart\n      [results]=\"data.os\"\n      [scheme]=\"colors\"\n      labels=\"true\"\n    ></ngx-charts-pie-chart>\n  </div>\n  <div class=\"half-chart\">\n    <div>Browsers</div>\n    <mat-divider></mat-divider>\n\n    <ngx-charts-bar-horizontal\n      [results]=\"data.platform\"\n      [scheme]=\"colors\"\n      yAxis=\"true\"\n    >\n    </ngx-charts-bar-horizontal>\n  </div>\n  <div class=\"half-chart\">\n    <div>Referers</div>\n    <mat-divider></mat-divider>\n    <ngx-charts-pie-chart\n      [results]=\"data.referer\"\n      [scheme]=\"colors\"\n      labels=\"true\"\n    >\n    </ngx-charts-pie-chart>\n  </div>\n  <div class=\"half-chart\">\n    <div>Today</div>\n    <mat-divider></mat-divider>\n    <div class=\"today\" *ngIf=\"data.timeline.series.length != 0\">\n      {{data.precentTodayClicks}}% | {{data.todayClicks}}\n    </div>\n\n  </div>\n</div>\n\n{{data.todayClicks | json}}\n"

/***/ }),

/***/ "./src/app/chart/chart.component.scss":
/*!********************************************!*\
  !*** ./src/app/chart/chart.component.scss ***!
  \********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".content-padding {\n  padding: 20px 210px;\n  background-color: #efeef3; }\n\n.about-link {\n  display: flex;\n  flex-direction: column; }\n\nspan {\n  font-weight: 300; }\n\nspan.header {\n  font-size: 1.2em;\n  margin: 5px 0; }\n\n.charts {\n  padding: 20px 210px;\n  display: flex;\n  flex-direction: row;\n  flex-flow: wrap; }\n\n.about-clicks {\n  display: flex;\n  flex-direction: column;\n  justify-content: center;\n  border: 0.5px black solid;\n  padding: 5px; }\n\n.timeframe {\n  display: flex;\n  flex-direction: column-reverse; }\n\n.timeframe-row {\n  display: flex;\n  flex-direction: row;\n  margin-left: 5px; }\n\n.total {\n  padding: 5px;\n  font-size: 0.8em;\n  margin: auto; }\n\nspan.clicks {\n  font-size: 1em;\n  font-weight: bold;\n  padding: 10px;\n  margin: auto; }\n\n.charts > .half-chart {\n  width: 50%;\n  min-height: 400px; }\n\n.charts > .full-chart {\n  width: 100%;\n  min-height: 300px; }\n\n.full-chart-about {\n  display: flex;\n  flex-direction: row;\n  flex-flow: wrap;\n  justify-content: space-between; }\n\n.today {\n  width: 100%;\n  padding-top: 25%;\n  text-align: center;\n  font-weight: bold;\n  font-size: 4em;\n  color: #3f51b5; }\n\n@media only screen and (max-width: 1000px) {\n  .charts > .half-chart {\n    width: 100%;\n    min-height: 400px; }\n  .charts > .full-chart {\n    width: 100%;\n    min-height: 300px; }\n  .charts {\n    padding: 40px 40px;\n    display: flex;\n    flex-direction: row;\n    flex-flow: wrap; } }\n"

/***/ }),

/***/ "./src/app/chart/chart.component.ts":
/*!******************************************!*\
  !*** ./src/app/chart/chart.component.ts ***!
  \******************************************/
/*! exports provided: ChartComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ChartComponent", function() { return ChartComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var ChartComponent = /** @class */ (function () {
    function ChartComponent() {
        this.colors = {
            domain: ['#3f51b5']
        };
    }
    ChartComponent.prototype.ngOnChanges = function () {
        Object.assign(this, this.data);
        this.clicks = this.getClicks();
        this.getTodayClicks();
        console.log(this.data);
    };
    ChartComponent.prototype.getClicks = function () {
        if (this.data.clicks) {
            return this.data.clicks;
        }
        return 0;
    };
    ChartComponent.prototype.getTodayClicks = function () {
        var today = new Date();
        today.setHours(0);
        today.setMinutes(0);
        if (this.data) {
            if (this.data.timeline.series.length > 1) {
                this.data.todayClicks = this.data.timeline.series.reduce(function (prev, curr) {
                    var recordTime = new Date(curr.name).getTime();
                    if (today.getTime() < recordTime) {
                        if (typeof prev === 'object') {
                            return prev.value + curr.value;
                        }
                        return prev + curr.value;
                    }
                });
                this.data.precentTodayClicks = Math.round((this.data.todayClicks * 100) / this.clicks);
            }
            else {
                this.data.todayClicks = this.clicks;
                this.data.precentTodayClicks = Math.round((this.data.timeline.series[0].value * 100) / this.clicks);
            }
        }
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], ChartComponent.prototype, "data", void 0);
    ChartComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-chart',
            template: __webpack_require__(/*! ./chart.component.html */ "./src/app/chart/chart.component.html"),
            styles: [__webpack_require__(/*! ./chart.component.scss */ "./src/app/chart/chart.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], ChartComponent);
    return ChartComponent;
}());



/***/ }),

/***/ "./src/app/header/header.component.html":
/*!**********************************************!*\
  !*** ./src/app/header/header.component.html ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"auth\">\n  <div class=\"auth-panel\">\n    <a *ngIf=\"!auth\" routerLink=\"/login\">login</a>\n    <a *ngIf=\"auth\">{{auth.username}}</a>\n    <a *ngIf=\"auth\" (click)=\"logout()\">logout</a>\n    <a *ngIf=\"!auth\" routerLink=\"/registration\">registration</a>\n  </div>\n</div>\n<div  class=\"flex-box\">\n  <div class=\"nav-bar-left-side\" routerLink=\"/\" >\n    <span style=\"color: dodgerblue\">Y</span>\n    <span style=\"color: red\">a</span>\n    <span style=\"color: yellow\">n</span>\n    <span style=\"color: dodgerblue\">d</span>\n    <span style=\"color: green\">e</span>\n    <span style=\"color: red\">x</span>\n  </div>\n  <div class=\"nav-bar-right-side\" routerLink=\"/\">URL Shortener</div>\n</div>\n"

/***/ }),

/***/ "./src/app/header/header.component.scss":
/*!**********************************************!*\
  !*** ./src/app/header/header.component.scss ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".nav-bar-left-side {\n  width: 200px;\n  justify-content: flex-end;\n  cursor: pointer; }\n\n.flex-box {\n  display: flex;\n  flex-wrap: wrap;\n  font-family: Roboto, \"Helvetica Neue\", sans-serif;\n  flex-direction: row;\n  padding: 2% 0; }\n\n.flex-box > div {\n  line-height: 75px;\n  font-size: 30px; }\n\ndiv.nav-bar-left-side {\n  text-align: end;\n  padding-right: 10px; }\n\n.nav-bar-right-side {\n  width: 10em;\n  cursor: pointer;\n  color: #757575; }\n\nspan {\n  font-family: Roboto, \"Helvetica Neue\", sans-serif; }\n\n.auth {\n  display: flex;\n  flex-direction: row;\n  flex-flow: wrap;\n  justify-content: flex-end; }\n\na {\n  margin: 0 10px;\n  cursor: pointer; }\n\n@media only screen and (max-width: 500px) {\n  div.nav-bar-left-side {\n    width: 100%;\n    text-align: center;\n    margin: auto; }\n  .nav-bar-right-side {\n    width: 100%;\n    text-align: center; } }\n"

/***/ }),

/***/ "./src/app/header/header.component.ts":
/*!********************************************!*\
  !*** ./src/app/header/header.component.ts ***!
  \********************************************/
/*! exports provided: HeaderComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HeaderComponent", function() { return HeaderComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _service_user_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../service/user.service */ "./src/app/service/user.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var HeaderComponent = /** @class */ (function () {
    function HeaderComponent(user) {
        this.user = user;
        this.auth = this.user.getUser();
    }
    HeaderComponent.prototype.ngOnInit = function () {
    };
    HeaderComponent.prototype.logout = function () {
        this.user.removeUser();
        window.location.reload();
    };
    HeaderComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-header',
            template: __webpack_require__(/*! ./header.component.html */ "./src/app/header/header.component.html"),
            styles: [__webpack_require__(/*! ./header.component.scss */ "./src/app/header/header.component.scss")]
        }),
        __metadata("design:paramtypes", [_service_user_service__WEBPACK_IMPORTED_MODULE_1__["UserService"]])
    ], HeaderComponent);
    return HeaderComponent;
}());



/***/ }),

/***/ "./src/app/input-field/input-field.component.html":
/*!********************************************************!*\
  !*** ./src/app/input-field/input-field.component.html ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"flex-box\">\n  <div class=\"title\">\n    Simplify your links\n  </div>\n  <div class=\"flex-description\">\n    <div class=\"flex-input\">\n      <input *ngIf=\"!auth\" routerLink=\"/login\" placeholder=\"Your original URL here\" [formControl]=\"shortInput\" type=\"text\">\n      <input *ngIf=\"auth\" placeholder=\"Your original URL here\" [formControl]=\"shortInput\" type=\"text\">\n      <button mat-raised-button (click)=\"generateUrl()\"  color=\"basic\">Shorter URL</button>\n      <span [hidden]=\"!status\">url is not correct</span>\n    </div>\n    <span class=\"description\"> All yand.ex URLs and click analytics are public and can be accessed by anyone</span>\n  </div>\n\n</div>\n"

/***/ }),

/***/ "./src/app/input-field/input-field.component.scss":
/*!********************************************************!*\
  !*** ./src/app/input-field/input-field.component.scss ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".flex-box {\n  display: flex;\n  flex-wrap: wrap;\n  font-family: Roboto, \"Helvetica Neue\", sans-serif;\n  font-weight: 100;\n  flex-direction: column;\n  padding-left: 210px;\n  background-color: #366ed1;\n  padding-top: 2em;\n  color: white;\n  padding-bottom: 2em; }\n\n.flex-box > div {\n  line-height: 50px;\n  font-size: 30px; }\n\n.flex-input {\n  display: flex;\n  flex-direction: row;\n  flex-flow: wrap;\n  font-family: Roboto, \"Helvetica Neue\", sans-serif;\n  font-weight: 100; }\n\n.flex-input > button {\n  margin-left: 10px; }\n\n.title {\n  padding: 20px 0; }\n\n.description {\n  font-size: 15px; }\n\n.flex-description {\n  display: flex;\n  flex-direction: column;\n  line-height: 1px; }\n\nbutton {\n  color: #366ed1; }\n\ninput {\n  width: 50%;\n  padding: 10px 10px;\n  font-size: 12px; }\n\n@media only screen and (max-width: 989px) {\n  .flex-box {\n    padding-left: 40px; } }\n\n@media only screen and (max-width: 736px) {\n  .flex-box {\n    padding: 40px; }\n  .flex-input {\n    width: 100%; }\n  .flex-input > button {\n    margin: 5px 0; }\n  input {\n    width: 100%; }\n  button {\n    width: 100%;\n    padding: 0;\n    margin: 0; } }\n"

/***/ }),

/***/ "./src/app/input-field/input-field.component.ts":
/*!******************************************************!*\
  !*** ./src/app/input-field/input-field.component.ts ***!
  \******************************************************/
/*! exports provided: InputFieldComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "InputFieldComponent", function() { return InputFieldComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _service_urls_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../service/urls.service */ "./src/app/service/urls.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _modal_preview_preview_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../modal/preview/preview.component */ "./src/app/modal/preview/preview.component.ts");
/* harmony import */ var _service_user_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../service/user.service */ "./src/app/service/user.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};







var InputFieldComponent = /** @class */ (function () {
    function InputFieldComponent(urls, navigate, dialog, user) {
        this.urls = urls;
        this.navigate = navigate;
        this.dialog = dialog;
        this.user = user;
        this.shortInput = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].minLength(8)]);
        this.auth = this.user.getUser();
    }
    InputFieldComponent.prototype.ngOnInit = function () {
    };
    InputFieldComponent.prototype.generateUrl = function () {
        var _this = this;
        if (this.shortInput.valid) {
            this.urls.generateURl(this.shortInput.value)
                .subscribe(function (url) { return __awaiter(_this, void 0, void 0, function () {
                var dil;
                return __generator(this, function (_a) {
                    dil = this.dialog.open(_modal_preview_preview_component__WEBPACK_IMPORTED_MODULE_5__["PreviewComponent"], {
                        data: url,
                        disableClose: true
                    });
                    return [2 /*return*/];
                });
            }); });
        }
    };
    InputFieldComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-input-field',
            template: __webpack_require__(/*! ./input-field.component.html */ "./src/app/input-field/input-field.component.html"),
            styles: [__webpack_require__(/*! ./input-field.component.scss */ "./src/app/input-field/input-field.component.scss")]
        }),
        __metadata("design:paramtypes", [_service_urls_service__WEBPACK_IMPORTED_MODULE_2__["UrlsService"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"], _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatDialog"], _service_user_service__WEBPACK_IMPORTED_MODULE_6__["UserService"]])
    ], InputFieldComponent);
    return InputFieldComponent;
}());



/***/ }),

/***/ "./src/app/interceptor/auth.interceptor.ts":
/*!*************************************************!*\
  !*** ./src/app/interceptor/auth.interceptor.ts ***!
  \*************************************************/
/*! exports provided: AuthInterceptor */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthInterceptor", function() { return AuthInterceptor; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var AuthInterceptor = /** @class */ (function () {
    function AuthInterceptor() {
    }
    AuthInterceptor.prototype.intercept = function (req, next) {
        var token = localStorage.getItem('token');
        if (token) {
            var request = req.clone({ setHeaders: { Authorization: 'Bearer ' + token } });
            return next.handle(request);
        }
        else {
            return next.handle(req);
        }
    };
    AuthInterceptor = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])(),
        __metadata("design:paramtypes", [])
    ], AuthInterceptor);
    return AuthInterceptor;
}());



/***/ }),

/***/ "./src/app/link-table/link-table.component.html":
/*!******************************************************!*\
  !*** ./src/app/link-table/link-table.component.html ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"table-links responsive_table mat-elevation-z1\">\n  <div >\n\n\n    <table mat-table [dataSource]=\"dataSource\">\n\n      <!-- Position Column -->\n      <ng-container matColumnDef=\"origin\">\n        <th mat-header-cell *matHeaderCellDef> Origin URL </th>\n        <td mat-cell *matCellDef=\"let element\"> <a  [href]=\"element.position\">{{element.origin}} </a> </td>\n      </ng-container>\n\n      <!-- Name Column -->\n      <ng-container matColumnDef=\"created\">\n        <th mat-header-cell *matHeaderCellDef> Created </th>\n        <td mat-cell *matCellDef=\"let element\"> {{element.time}} </td>\n      </ng-container>\n\n      <!-- Weight Column -->\n      <ng-container matColumnDef=\"short\">\n        <th mat-header-cell *matHeaderCellDef> Short URL </th>\n        <td mat-cell *matCellDef=\"let element\">\n          <a href=\"/q/{{element.shortCode}}\">yand.ex/q/{{element.shortCode}}</a>\n          <!--<mat-icon>info</mat-icon>-->\n        </td>\n      </ng-container>\n\n      <!-- Symbol Column -->\n      <ng-container matColumnDef=\"clicks\">\n        <th mat-header-cell *matHeaderCellDef> Actions </th>\n        <td mat-cell *matCellDef=\"let element\">\n          <div class=\"menu-setting\">\n            <div>\n              <button mat-icon-button [matMenuTriggerFor]=\"menu\">\n                <mat-icon>more_vert</mat-icon>\n              </button>\n              <mat-menu #menu=\"matMenu\">\n                <button (click)=\"analyticView(element)\" mat-menu-item>\n                  <span>Analytics Data</span>\n                </button>\n                <button mat-menu-item (click)=\"getQR(element.shortCode)\">\n                  <span>QR Code</span>\n                </button>\n              </mat-menu>\n            </div>\n          </div>\n        </td>\n      </ng-container>\n\n      <tr mat-header-row *matHeaderRowDef=\"columns\"></tr>\n      <tr mat-row *matRowDef=\"let row; columns: columns;\"></tr>\n    </table>\n    <mat-paginator [pageSizeOptions]=\"[10]\" showFirstLastButtons></mat-paginator>\n\n  </div>\n\n</div>\n"

/***/ }),

/***/ "./src/app/link-table/link-table.component.scss":
/*!******************************************************!*\
  !*** ./src/app/link-table/link-table.component.scss ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "table {\n  width: 100%; }\n\n.menu-setting {\n  display: flex;\n  flex-direction: row;\n  justify-content: space-between; }\n\na {\n  color: #29b6f6; }\n\ndiv {\n  width: auto; }\n\n.table-links {\n  margin: 20px 211px;\n  padding-top: 10px; }\n\n@media only screen and (max-width: 500px) {\n  .responsive_table {\n    overflow-x: auto !important; }\n  .mat-table {\n    min-width: 150px !important; } }\n\n@media only screen and (max-width: 989px) {\n  .table-links {\n    margin: 40px 40px; } }\n\n@media only screen and (max-width: 736px) {\n  .table-links {\n    margin: 40px 40px; }\n  table {\n    width: 100%; } }\n"

/***/ }),

/***/ "./src/app/link-table/link-table.component.ts":
/*!****************************************************!*\
  !*** ./src/app/link-table/link-table.component.ts ***!
  \****************************************************/
/*! exports provided: LinkTableComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LinkTableComponent", function() { return LinkTableComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _service_urls_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../service/urls.service */ "./src/app/service/urls.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _modal_preview_preview_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../modal/preview/preview.component */ "./src/app/modal/preview/preview.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};





var LinkTableComponent = /** @class */ (function () {
    function LinkTableComponent(url, navigator, dialog) {
        var _this = this;
        this.navigator = navigator;
        this.dialog = dialog;
        this.columns = ['origin', 'created', 'short', 'clicks'];
        url.getUrls()
            .then(function (result) {
            result.subscribe(function (res) {
                _this.dataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatTableDataSource"](res.result);
                _this.dataSource.paginator = _this.paginator;
            });
        });
    }
    LinkTableComponent.prototype.ngOnInit = function () {
    };
    LinkTableComponent.prototype.analyticView = function (data) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.navigator.navigateByUrl("/analytics/" + data.shortCode)];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    LinkTableComponent.prototype.getQR = function (url) {
        var dialog = this.dialog.open(_modal_preview_preview_component__WEBPACK_IMPORTED_MODULE_4__["PreviewComponent"], {
            data: {
                shortCode: url
            },
            disableClose: true,
        });
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_1__["MatPaginator"]),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatPaginator"])
    ], LinkTableComponent.prototype, "paginator", void 0);
    LinkTableComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-link-table',
            template: __webpack_require__(/*! ./link-table.component.html */ "./src/app/link-table/link-table.component.html"),
            styles: [__webpack_require__(/*! ./link-table.component.scss */ "./src/app/link-table/link-table.component.scss")]
        }),
        __metadata("design:paramtypes", [_service_urls_service__WEBPACK_IMPORTED_MODULE_2__["UrlsService"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"], _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatDialog"]])
    ], LinkTableComponent);
    return LinkTableComponent;
}());



/***/ }),

/***/ "./src/app/login/login.component.html":
/*!********************************************!*\
  !*** ./src/app/login/login.component.html ***!
  \********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<mat-card>\n  <p align=\"center\" class=\"logo\" routerLink=\"/\">\n    <span style=\"color: dodgerblue\">Y</span>\n    <span style=\"color: red\">a</span>\n    <span style=\"color: yellow\">n</span>\n    <span style=\"color: dodgerblue\">d</span>\n    <span style=\"color: green\">e</span>\n    <span style=\"color: red\">x</span>\n  </p>\n  <p  align=\"center\" class=\"sign-in\">Вход</p>\n  <p align=\"center\" class=\"google-account\">Используйте аккаунт Google</p>\n  <form class=\"form\">\n\n    <mat-form-field id=\"login\" class=\"full-width\">\n\n      <input  matInput placeholder=\"Адрес эл.почты\" [formControl]=\"login\">\n\n    </mat-form-field>\n    <label *ngIf=\"!login.valid && press\" for=\"login\">\n      Not correct email\n    </label>\n\n    <label *ngIf=\"press && status\" for=\"login\">\n      Parol' ili email is not correct!\n    </label>\n\n    <mat-form-field class=\"full-width\">\n      <input id=\"password\" matInput placeholder=\"Пароль\" type=\"password\" [formControl]=\"password\">\n    </mat-form-field>\n    <label *ngIf=\"!password.valid && press\" for=\"password\">\n      min 8\n    </label>\n  </form>\n  <span class=\"google-secure\">Работаете на чужом компьютере? Включите гостевой режим.\n    <a href=\"https://support.google.com/chrome/answer/6130773?hl=ru\">Подробнее…</a></span>\n\n  <div class=\"buttons\">\n    <button mat-button color=\"primary\" routerLink=\"/registration\">Создать аккаунт</button>\n    <button  mat-raised-button color=\"primary\" (click)=\"singIn()\">Войти</button>\n  </div>\n</mat-card>\n\n"

/***/ }),

/***/ "./src/app/login/login.component.scss":
/*!********************************************!*\
  !*** ./src/app/login/login.component.scss ***!
  \********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "mat-card {\n  width: 25%;\n  margin: 10% auto;\n  align-content: center;\n  padding: 50px; }\n\n.logo {\n  cursor: pointer;\n  font-size: 2em; }\n\n.full-width {\n  width: 100%;\n  margin: 20px 0; }\n\n.form {\n  width: 100%;\n  line-height: 0;\n  margin: 20px 0; }\n\nlabel {\n  color: red; }\n\n.sign-in {\n  font-size: 2em; }\n\n.google-account {\n  font-size: 1.2em; }\n\n.google-secure {\n  color: #5f6368;\n  margin: 40px 0; }\n\n.buttons {\n  display: flex;\n  flex-direction: row;\n  justify-content: space-between;\n  flex-flow: wrap;\n  margin: 40px 0; }\n\n@media only screen and (min-width: 1600px) {\n  mat-card {\n    width: 20%; } }\n\n@media only screen and (max-width: 1024px) {\n  mat-card {\n    width: 30%; } }\n\n@media only screen and (max-width: 836px) {\n  mat-card {\n    width: 40%; } }\n\n@media only screen and (max-width: 668px) {\n  mat-card {\n    width: 50%; } }\n"

/***/ }),

/***/ "./src/app/login/login.component.ts":
/*!******************************************!*\
  !*** ./src/app/login/login.component.ts ***!
  \******************************************/
/*! exports provided: LoginComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginComponent", function() { return LoginComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _service_auth_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../service/auth.service */ "./src/app/service/auth.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _service_user_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../service/user.service */ "./src/app/service/user.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};





var LoginComponent = /** @class */ (function () {
    function LoginComponent(auth, navigate, userService) {
        this.auth = auth;
        this.navigate = navigate;
        this.userService = userService;
        this.login = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].email]);
        this.password = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].minLength(8)]);
    }
    LoginComponent.prototype.ngOnInit = function () {
    };
    LoginComponent.prototype.singIn = function () {
        var _this = this;
        this.press = true;
        if (this.login.valid && this.password.valid) {
            var login = this.auth.login(this.login.value, this.password.value);
            login.subscribe(function (user) { return __awaiter(_this, void 0, void 0, function () {
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0:
                            if (!user.status) return [3 /*break*/, 2];
                            localStorage.setItem('token', user.token);
                            this.userService.setUser(user);
                            return [4 /*yield*/, this.navigate.navigate(['/'])];
                        case 1:
                            _a.sent();
                            return [3 /*break*/, 3];
                        case 2:
                            this.status = true;
                            _a.label = 3;
                        case 3: return [2 /*return*/];
                    }
                });
            }); }, function (error1) {
                _this.login.reset();
                _this.password.reset();
            });
        }
    };
    LoginComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-login',
            template: __webpack_require__(/*! ./login.component.html */ "./src/app/login/login.component.html"),
            styles: [__webpack_require__(/*! ./login.component.scss */ "./src/app/login/login.component.scss")]
        }),
        __metadata("design:paramtypes", [_service_auth_service__WEBPACK_IMPORTED_MODULE_2__["AuthService"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"], _service_user_service__WEBPACK_IMPORTED_MODULE_4__["UserService"]])
    ], LoginComponent);
    return LoginComponent;
}());



/***/ }),

/***/ "./src/app/main/main.component.html":
/*!******************************************!*\
  !*** ./src/app/main/main.component.html ***!
  \******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-header></app-header>\n<app-input-field></app-input-field>\n<app-link-table *ngIf=\"user.getUser()\"></app-link-table>\n<app-about *ngIf=\"!user.getUser()\"></app-about>\n"

/***/ }),

/***/ "./src/app/main/main.component.scss":
/*!******************************************!*\
  !*** ./src/app/main/main.component.scss ***!
  \******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/main/main.component.ts":
/*!****************************************!*\
  !*** ./src/app/main/main.component.ts ***!
  \****************************************/
/*! exports provided: MainComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MainComponent", function() { return MainComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _service_user_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../service/user.service */ "./src/app/service/user.service.ts");
/* harmony import */ var _service_auth_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../service/auth.service */ "./src/app/service/auth.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var MainComponent = /** @class */ (function () {
    function MainComponent(user, auth) {
        var _this = this;
        this.user = user;
        this.auth = auth;
        var token = localStorage.getItem('token');
        this.auth.status(token)
            .subscribe(function (value) {
            _this.user.setUser(value);
        });
    }
    MainComponent.prototype.ngOnInit = function () {
    };
    MainComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-main',
            template: __webpack_require__(/*! ./main.component.html */ "./src/app/main/main.component.html"),
            styles: [__webpack_require__(/*! ./main.component.scss */ "./src/app/main/main.component.scss")],
        }),
        __metadata("design:paramtypes", [_service_user_service__WEBPACK_IMPORTED_MODULE_1__["UserService"], _service_auth_service__WEBPACK_IMPORTED_MODULE_2__["AuthService"]])
    ], MainComponent);
    return MainComponent;
}());



/***/ }),

/***/ "./src/app/modal/preview/preview.component.html":
/*!******************************************************!*\
  !*** ./src/app/modal/preview/preview.component.html ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div *ngIf=\"data.shortCode\">\n  <h4>Short URL</h4>\n\n  <a [href]=\"host\">{{data.shortCode}}</a>\n  <div>\n    <qrcode [qrdata]=\"host\"></qrcode>\n  </div>\n\n  <div class=\"actions\">\n    <button mat-raised-button color=\"primary\" (click)=\"closeDialog()\">Close</button>\n  </div>\n\n</div>\n<div *ngIf=\"!data.shortCode\">\n  <mat-icon align=\"center\">warning</mat-icon>\n  <h1>URL is exist</h1>\n\n  <!--<a [href]=\"host\">{{data.shortCode}}</a>-->\n  <!--<div>-->\n    <!--<qrcode [qrdata]=\"host\"></qrcode>-->\n  <!--</div>-->\n\n  <div class=\"actions\">\n    <button mat-raised-button class=\"button-close\" color=\"primary\" (click)=\"closeDialog()\">close</button>\n  </div>\n\n</div>\n"

/***/ }),

/***/ "./src/app/modal/preview/preview.component.scss":
/*!******************************************************!*\
  !*** ./src/app/modal/preview/preview.component.scss ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "a {\n  margin: 20px 0; }\n\ndiv {\n  margin: 20px 0 !important; }\n\n.actions {\n  display: flex;\n  flex-flow: wrap;\n  flex-direction: row;\n  justify-content: flex-end; }\n\ndiv {\n  color: #767676; }\n\n.button-close {\n  width: 100%; }\n"

/***/ }),

/***/ "./src/app/modal/preview/preview.component.ts":
/*!****************************************************!*\
  !*** ./src/app/modal/preview/preview.component.ts ***!
  \****************************************************/
/*! exports provided: PreviewComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PreviewComponent", function() { return PreviewComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (undefined && undefined.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};



var PreviewComponent = /** @class */ (function () {
    function PreviewComponent(dialogRef, data, router) {
        this.dialogRef = dialogRef;
        this.data = data;
        this.router = router;
        this.host = '/q/' + this.data.shortCode;
    }
    PreviewComponent.prototype.ngOnInit = function () {
    };
    PreviewComponent.prototype.closeDialog = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                this.dialogRef.close();
                window.location.reload();
                return [2 /*return*/];
            });
        });
    };
    PreviewComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-preview',
            template: __webpack_require__(/*! ./preview.component.html */ "./src/app/modal/preview/preview.component.html"),
            styles: [__webpack_require__(/*! ./preview.component.scss */ "./src/app/modal/preview/preview.component.scss")]
        }),
        __param(1, Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"])(_angular_material__WEBPACK_IMPORTED_MODULE_1__["MAT_DIALOG_DATA"])),
        __metadata("design:paramtypes", [_angular_material__WEBPACK_IMPORTED_MODULE_1__["MatDialogRef"], Object, _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]])
    ], PreviewComponent);
    return PreviewComponent;
}());



/***/ }),

/***/ "./src/app/registration/registration.component.html":
/*!**********************************************************!*\
  !*** ./src/app/registration/registration.component.html ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<mat-card>\n  <p align=\"center\" class=\"logo\" routerLink=\"/\">\n    <span style=\"color: dodgerblue\">Y</span>\n    <span style=\"color: red\">a</span>\n    <span style=\"color: yellow\">n</span>\n    <span style=\"color: dodgerblue\">d</span>\n    <span style=\"color: green\">e</span>\n    <span style=\"color: red\">x</span>\n  </p>\n  <p  align=\"center\" class=\"sign-in\">Регистрация</p>\n  <p align=\"center\" class=\"google-account\">Создайте аккаунт Яндекс</p>\n  <form class=\"form\">\n\n    <mat-form-field id=\"login\" class=\"full-width\">\n\n      <input  matInput placeholder=\"Адрес эл.почты\" [formControl]=\"login\">\n\n    </mat-form-field>\n    <label *ngIf=\"!login.valid && press\" for=\"login\">\n      Not correct email\n    </label>\n    <label *ngIf=\"press && status\" for=\"login\">\n      Email is exist\n    </label>\n\n    <mat-form-field class=\"full-width\">\n      <input id=\"password\" matInput placeholder=\"Пароль\" type=\"password\" [formControl]=\"password\">\n    </mat-form-field>\n    <label *ngIf=\"!password.valid && press\" for=\"password\">\n      min 8\n    </label>\n  </form>\n\n\n  <div class=\"buttons\">\n    <button mat-button color=\"primary\" routerLink=\"/login\">Войти</button>\n    <button  mat-raised-button color=\"primary\" (click)=\"singUp()\">Создать аккаунт</button>\n  </div>\n</mat-card>\n\n"

/***/ }),

/***/ "./src/app/registration/registration.component.scss":
/*!**********************************************************!*\
  !*** ./src/app/registration/registration.component.scss ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "mat-card {\n  width: 25%;\n  margin: 10% auto;\n  align-content: center;\n  padding: 50px; }\n\n.logo {\n  cursor: pointer;\n  font-size: 2em; }\n\n.full-width {\n  width: 100%;\n  margin: 20px 0; }\n\n.form {\n  width: 100%;\n  line-height: 0;\n  margin: 20px 0; }\n\nlabel {\n  color: red; }\n\n.sign-in {\n  font-size: 2em; }\n\n.google-account {\n  font-size: 1.2em; }\n\n.google-secure {\n  color: #5f6368;\n  margin: 40px 0; }\n\n.buttons {\n  display: flex;\n  flex-direction: row;\n  justify-content: space-between;\n  flex-flow: wrap;\n  margin: 40px 0; }\n\n@media only screen and (min-width: 1600px) {\n  mat-card {\n    width: 20%; } }\n\n@media only screen and (max-width: 1024px) {\n  mat-card {\n    width: 30%; } }\n\n@media only screen and (max-width: 836px) {\n  mat-card {\n    width: 40%; } }\n\n@media only screen and (max-width: 668px) {\n  mat-card {\n    width: 50%; } }\n"

/***/ }),

/***/ "./src/app/registration/registration.component.ts":
/*!********************************************************!*\
  !*** ./src/app/registration/registration.component.ts ***!
  \********************************************************/
/*! exports provided: RegistrationComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RegistrationComponent", function() { return RegistrationComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _service_auth_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../service/auth.service */ "./src/app/service/auth.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _service_user_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../service/user.service */ "./src/app/service/user.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};





var RegistrationComponent = /** @class */ (function () {
    function RegistrationComponent(auth, navigate, userService) {
        this.auth = auth;
        this.navigate = navigate;
        this.userService = userService;
        this.login = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].email]);
        this.password = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].minLength(8)]);
    }
    RegistrationComponent.prototype.ngOnInit = function () {
    };
    RegistrationComponent.prototype.singUp = function () {
        var _this = this;
        this.press = true;
        if (this.login.valid && this.password.valid) {
            var registration = this.auth.registration(this.login.value, this.password.value);
            registration.subscribe(function (user) { return __awaiter(_this, void 0, void 0, function () {
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0:
                            if (!user.status) return [3 /*break*/, 2];
                            localStorage.setItem('token', user.token);
                            this.userService.setUser(user);
                            return [4 /*yield*/, this.navigate.navigate(['/'])];
                        case 1:
                            _a.sent();
                            return [3 /*break*/, 3];
                        case 2:
                            this.status = true;
                            _a.label = 3;
                        case 3: return [2 /*return*/];
                    }
                });
            }); }, function (error1) { return console.error(error1); });
        }
    };
    RegistrationComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-registration',
            template: __webpack_require__(/*! ./registration.component.html */ "./src/app/registration/registration.component.html"),
            styles: [__webpack_require__(/*! ./registration.component.scss */ "./src/app/registration/registration.component.scss")]
        }),
        __metadata("design:paramtypes", [_service_auth_service__WEBPACK_IMPORTED_MODULE_2__["AuthService"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"], _service_user_service__WEBPACK_IMPORTED_MODULE_4__["UserService"]])
    ], RegistrationComponent);
    return RegistrationComponent;
}());



/***/ }),

/***/ "./src/app/service/auth.service.ts":
/*!*****************************************!*\
  !*** ./src/app/service/auth.service.ts ***!
  \*****************************************/
/*! exports provided: AuthService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthService", function() { return AuthService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../environments/environment */ "./src/environments/environment.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var AuthService = /** @class */ (function () {
    function AuthService(http) {
        this.http = http;
        this.host = _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].host;
        this.api = _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].api;
        this.version = _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version;
    }
    AuthService.prototype.login = function (login, password) {
        return this.http.post(this.host + "/api/auth/sign-in/", { email: login, password: password });
    };
    AuthService.prototype.registration = function (login, password) {
        return this.http.post(this.host + "/api/auth/sign-up/", { email: login, password: password });
    };
    AuthService.prototype.status = function (token) {
        return this.http.post(this.host + "/api/auth/status/", { token: token });
    };
    AuthService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
    ], AuthService);
    return AuthService;
}());



/***/ }),

/***/ "./src/app/service/charts.service.ts":
/*!*******************************************!*\
  !*** ./src/app/service/charts.service.ts ***!
  \*******************************************/
/*! exports provided: ChartsService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ChartsService", function() { return ChartsService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../environments/environment */ "./src/environments/environment.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};



var ChartsService = /** @class */ (function () {
    function ChartsService(http) {
        this.http = http;
        this.host = _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].host;
        this.api = _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].api;
        this.version = _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].version;
    }
    ChartsService.prototype.getData = function (id) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.http.get(this.host + "/" + this.api + "/account/analytic/" + id)];
                    case 1: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    ChartsService.prototype.getPage = function (id) {
        return this.http.get(this.host + "/" + this.api + "/account/analytic-page/" + id);
    };
    ChartsService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
    ], ChartsService);
    return ChartsService;
}());



/***/ }),

/***/ "./src/app/service/urls.service.ts":
/*!*****************************************!*\
  !*** ./src/app/service/urls.service.ts ***!
  \*****************************************/
/*! exports provided: UrlsService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UrlsService", function() { return UrlsService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../environments/environment */ "./src/environments/environment.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};



var UrlsService = /** @class */ (function () {
    function UrlsService(http) {
        this.http = http;
        this.host = _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].host;
        this.api = _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].api;
    }
    UrlsService.prototype.getUrls = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.http.get(this.host + "/" + this.api + "/account")];
                    case 1: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    UrlsService.prototype.generateURl = function (url) {
        return this.http.post(this.host + "/" + this.api + "/account", { 'origin': url });
    };
    UrlsService.prototype.checkUrl = function (url) {
        return this.http.get(url);
    };
    UrlsService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
    ], UrlsService);
    return UrlsService;
}());



/***/ }),

/***/ "./src/app/service/user.service.ts":
/*!*****************************************!*\
  !*** ./src/app/service/user.service.ts ***!
  \*****************************************/
/*! exports provided: UserService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserService", function() { return UserService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var UserService = /** @class */ (function () {
    function UserService() {
    }
    UserService.prototype.getUser = function () {
        var user = localStorage.getItem('user');
        return JSON.parse(user);
    };
    UserService.prototype.setUser = function (user) {
        localStorage.setItem('user', JSON.stringify(user));
        this.user = user;
    };
    UserService.prototype.removeUser = function () {
        localStorage.clear();
    };
    UserService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [])
    ], UserService);
    return UserService;
}());



/***/ }),

/***/ "./src/app/signup/signup.component.html":
/*!**********************************************!*\
  !*** ./src/app/signup/signup.component.html ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<p>\n  signup works!\n</p>\n"

/***/ }),

/***/ "./src/app/signup/signup.component.scss":
/*!**********************************************!*\
  !*** ./src/app/signup/signup.component.scss ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/signup/signup.component.ts":
/*!********************************************!*\
  !*** ./src/app/signup/signup.component.ts ***!
  \********************************************/
/*! exports provided: SignupComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SignupComponent", function() { return SignupComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var SignupComponent = /** @class */ (function () {
    function SignupComponent() {
    }
    SignupComponent.prototype.ngOnInit = function () {
    };
    SignupComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-signup',
            template: __webpack_require__(/*! ./signup.component.html */ "./src/app/signup/signup.component.html"),
            styles: [__webpack_require__(/*! ./signup.component.scss */ "./src/app/signup/signup.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], SignupComponent);
    return SignupComponent;
}());



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
var environment = {
    production: false,
    host: '',
    api: 'api',
    version: 'v1',
};
/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var hammerjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! hammerjs */ "./node_modules/hammerjs/hammer.js");
/* harmony import */ var hammerjs__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(hammerjs__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");





if (_environments_environment__WEBPACK_IMPORTED_MODULE_4__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_3__["AppModule"])
    .catch(function (err) { return console.log(err); });


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /Users/altynbekkaliakbarov/WebstormProjects/roowix/shortner/front-end/src/main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map